#ifndef ITEM_H_
#define ITEM_H_

#include "Block.h"
#include "Util.h"

class Item
{
public:
	static std::vector<Item> makeBlockItem(GLuint itemTex, GLuint tex, Block::Type type, sf::Vector3f pos)
	{
		std::vector<Item> items;
		sf::Vector2f texPos(260.0/1024.0, 650.0/2048.0);
		std::string name = "stone";

		bool blockDrop = true;

		if(type == Block::Grass || type == Block::Dirt)
		{
			type = Block::Dirt;
			texPos = sf::Vector2f(650.0/1024.0, 130.0/2048.0);
			name = "dirt";

		}
		else if(type == Block::Brick)
		{
			texPos = sf::Vector2f(260.0/1024.0, 1560.0/2048.0);
			name = "brick";

		}
		else if(type == Block::Wood)
		{
			texPos = sf::Vector2f(0.0, 260.0/2048.0);
			name = "wood";

		}
		else if(type == Block::Log)
		{
			texPos = sf::Vector2f(0.0, 1430.0/2048.0);
			name = "log";

		}
		else if(type == Block::Leaf)
		{
			if(rand()%6 == 0)
			{
				items.push_back(Item(pos, itemTex, sf::Vector2f(390.0/1024.0, 260.0/2048.0), "apple"));

			}

			texPos = sf::Vector2f(390/1024.0, 520.0/2048.0);
			name = "leaf";

		}
		else if(type == Block::Coal)
		{
			blockDrop = false;
			items.push_back(Item(pos, itemTex, sf::Vector2f(130/1024.0, 260/2048.0), "coal"));

		}

		if(blockDrop)
		{
			Item it(pos, tex, texPos, name, true);
			it.type = type;
			items.push_back(it);

		}

		return items;

	}

	Item()
	{
	}

	Item(sf::Vector3f pos,
			GLuint tex,
			sf::Vector2f texPos,
			std::string name,
			bool isBlock = false) :
				pos(pos),
				tex(tex),
				texPos(texPos),
				name(name),
				isBlock(isBlock)
	{
		bounceUp = false;
		bounce = 0.0;
		angle = 0.0f;
		quantity = 1;
		aabb = AABB(pos, sf::Vector3f(0.15, 0.15, 0.15));


		vel.x = (float)(4 - rand()%8)/24.0f;
		vel.z = (float)(4 - rand()%8)/24.0f;
		vel.y = (float)(rand()%4)/12.0f;

		hash = computeHash(pos);
		//std::cout << "item hash: " << hash << "\n";

	}

	void update()
	{
		angle += 0.01;
		pos.y -= 0.15;
		pos += vel;
		vel *= 0.93f;
		aabb.c = pos;

		if(bounceUp && bounce < 0.25)
		{
			bounce += 0.01;

		}
		else if(bounceUp && bounce >= 0.25)
		{
			bounceUp = false;

		}
		else if(!bounceUp && bounce > 0.0)
		{
			bounce -= 0.01;

		}
		else if(!bounceUp && bounce <= 0.0)
		{
			bounceUp = true;

		}

		//hash = computeHash(pos);

	}

	void draw()
	{
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		sf::Vector2f tsize(128.0/1024.0, 128/2048.0);

		if(isBlock)
		{
			glColor3f(1.0, 1.0, 1.0);

			float size = 0.15;

			glPushMatrix();
			glTranslatef(pos.x, pos.y+0.3+bounce, pos.z);
			glRotatef(angle*30.0, 1.0, 0.0, 0.0);
			glRotatef(angle*60.0, 0.0, 1.0, 0.0);
			glRotatef(angle*90.0, 0.0, 0.0, 1.0);

			glBegin(GL_QUADS);

			//front
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(-size, -size, size);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(-size, size, size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(size, size, size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(size, -size, size);

			//right
			glNormal3f(1.0f, 0.0f, 0.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(size, size, size);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(size, size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(size, -size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(size, -size, size);

			//backwindow.draw()
			glNormal3f(0.0f, 0.0f, -1.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(-size, -size, -size);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(-size, size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(size, size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(size, -size, -size);

			//left
			glNormal3f(-1.0f, 0.0f, 0.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(-size, size, size);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(-size, size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(-size, -size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(-size, -size, size);

			//top
			glNormal3f(0.0f, 1.0f, 0.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(size, size, size);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(-size, size, size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(-size, size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(size, size, -size);

			//bottom
			glNormal3f(0.0f, -1.0f, 0.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(size, -size, size);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(-size, -size, size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(-size, -size, -size);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(size, -size, -size);

			glEnd();

			glPopMatrix();

		}
		else
		{
			glColor3f(1.0, 1.0, 1.0);
			glPushMatrix();
			float size = 0.15;
			glTranslatef(pos.x, pos.y+0.3+bounce, pos.z);
			glRotatef(180.0, 1.0, 0.0, 0.0);
			glRotatef(angle*60.0, 0.0, 1.0, 0.0);

			glBegin(GL_QUADS);

			//front
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord2f(texPos.x, texPos.y);
			glVertex3f(-size, -size, 0.0);
			glTexCoord2f(texPos.x, texPos.y+tsize.y);
			glVertex3f(-size, size, 0.0);
			glTexCoord2f(texPos.x+tsize.x, texPos.y+tsize.y);
			glVertex3f(size, size, 0.0);
			glTexCoord2f(texPos.x+tsize.x, texPos.y);
			glVertex3f(size, -size, 0.0);

			glEnd();
			glPopMatrix();

		}

	}

	void resolve(AABB& intersection)
	{
		sf::Vector3f mtv = intersection.getMTV();
		if(intersection.h.x != 0
				&& mtv.x == intersection.h.x)
		{
			if(pos.x > intersection.c.x)
				pos.x += intersection.h.x;
			else
				pos.x -= intersection.h.x;

		}

		if(intersection.h.z != 0
				&& mtv.z == intersection.h.z)
		{
			if(pos.z > intersection.c.z)
				pos.z += intersection.h.z;
			else
				pos.z -= intersection.h.z;

		}

		if(intersection.h.y != 0
				&& mtv.y == intersection.h.y)
		{
			if(pos.y > intersection.c.y)
				pos.y += intersection.h.y;
			else
				pos.y -= intersection.h.y;

		}

		aabb.c = pos;


	}


	sf::Clock lifeTimer;
	float angle;
	bool bounceUp;
	float bounce;
	sf::Vector3f pos;
	AABB aabb;
	GLuint tex;
	sf::Vector2f texPos;
	std::string name;
	bool isBlock;
	int quantity;
	sf::Vector3f vel;
	Block::Type type;

	unsigned int hash;

};

#endif /* ITEM_H_ */
