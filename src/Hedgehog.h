#ifndef HEDGEHOG_H_
#define HEDGEHOG_H_

#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "Util.h"
#include "Chunk.h"

#define PI 3.14145

class Hedgehog
{
public:
	Hedgehog(sf::Vector3f pos) :
		pos(pos),
		target(pos)
	{
		localPlayer = false;
		aabb = AABB(pos, sf::Vector3f(0.3, 0.3, 0.3));
		walkSpeed = 0.1f;
		jumping = false;
		jump = 0;
		jumpHeight = 0.2f;

		thirdPerson = false;

		footStepBuffer.loadFromFile("footstep.ogg");
		footStepSound.setBuffer(footStepBuffer);
		footStepSound.setVolume(75);

		footStepTimer.restart();
		recomputeHash();
		changeVisible = true;

	}

	sf::Vector3f getCamPos(sf::Vector3f camRot)
	{
		sf::Vector3f camPos = pos;
		camPos *= -1.0f;
		camPos.y -= 0.5f;

		if(thirdPerson)
		{
			camPos.y -= 2.0f;
			float yrot = camRot.y/180.0f*PI;
			camPos.x += 1.5*sin(yrot);
			camPos.z -= 1.5*cos(yrot);

		}

		return camPos;

	}

	void draw()
	{
		/*glColor4f(1.0, 1.0, 1.0, 0.5);
		drawCube(sf::Vector3f(pos.x, pos.y, pos.z), 0.3);*/

		glPushMatrix();
		glTranslatef(pos.x, pos.y, pos.z);

		glRotatef(rot.x, 0.0, 1.0, 0.0);
		glRotatef(rot.y, 0.0, 0.0, 1.0);

		glBegin(GL_QUADS);

		glColor4f(1.0, 1.0, 1.0, 1.0);
		glNormal3f(0.0, 0.0, 0.1);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(0.28125, 0.3125, 0.0);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(0.28125, 0.0, 0.0);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-0.28125, 0.0, 0.0);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-0.28125, 0.3125, 0.0);

		glEnd();

		glPopMatrix();

	}

	void update(bool focus,
			bool paused,
			sf::Vector3f camRot)
	{
		if(!jumping || (jumping && jump >= 9))
		{
			pos.y -= 0.25f;

		}
		else
		{
			jump++;
			pos.y += jumpHeight;

		}

		if(!localPlayer)
		{
			pos += (target - pos)*0.2f;

		}
		else
		{
			float dPosX = pos.x;

			if(!paused && focus && sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				float yrot = camRot.y/180.0f*PI;
				pos.x += walkSpeed*sin(yrot);
				pos.z -= walkSpeed*cos(yrot);

			}

			if(!paused && focus && sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				float yrot = camRot.y/180.0f*PI;
				pos.x -= walkSpeed*sin(yrot);
				pos.z += walkSpeed*cos(yrot);

			}

			if(!paused && focus && sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				float yrot = camRot.y/180.0f*PI;
				pos.x -= walkSpeed*cos(yrot);
				pos.z -= walkSpeed*sin(yrot);

			}

			if(!paused && focus && sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				float yrot = camRot.y/180.0f*PI;
				pos.x += walkSpeed*cos(yrot);
				pos.z += walkSpeed*sin(yrot);

			}

			if(!paused && focus && !jumping && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				jumping = true;
				jump = 0;

			}

			if(dPosX != pos.x && footStepTimer.getElapsedTime().asSeconds() > 1)
			{
				footStepSound.play();
				footStepTimer.restart();
			}

		}

		aabb.c = pos;
		recomputeHash();

	}

	void resolve(AABB& intersection)
	{
		sf::Vector3f mtv = intersection.getMTV();
		if(intersection.h.x != 0
				&& mtv.x == intersection.h.x)
		{
			if(pos.x > intersection.c.x)
				pos.x += intersection.h.x;
			else
				pos.x -= intersection.h.x;

		}

		if(intersection.h.z != 0
				&& mtv.z == intersection.h.z)
		{
			if(pos.z > intersection.c.z)
				pos.z += intersection.h.z;
			else
				pos.z -= intersection.h.z;

		}

		if(intersection.h.y != 0
				&& mtv.y == intersection.h.y)
		{
			if(pos.y > intersection.c.y)
			{
				pos.y += intersection.h.y;
				if(jump >= 9)
					jumping = false;

			}
			else
			{
				pos.y -= intersection.h.y;
				//jumping = false;

			}


		}

		aabb.c = pos;

	}

	void recomputeHash()
	{
		float add = walkSpeed*(CHUNK_SIZE+4);
		unsigned int nhash = computeHash(pos);
		if(chash != nhash)
		{
			chash = nhash;
			changeVisible = true;


		}

		fhash = computeHash(sf::Vector2f(pos.x, pos.z+add));
		bhash = computeHash(sf::Vector2f(pos.x, pos.z-add));
		rhash = computeHash(sf::Vector2f(pos.x+add, pos.z));
		lhash = computeHash(sf::Vector2f(pos.x-add, pos.z));
		lfhash = computeHash(sf::Vector2f(pos.x-add, pos.z+add));
		rfhash = computeHash(sf::Vector2f(pos.x+add, pos.z+add));
		lbhash = computeHash(sf::Vector2f(pos.x-add, pos.z-add));
		rbhash = computeHash(sf::Vector2f(pos.x+add, pos.z-add));

	}

	bool checkCollision(unsigned int chunkHash)
	{
		return chunkHash == chash || chunkHash == fhash || chunkHash == bhash
				|| chunkHash == rhash || chunkHash == lhash || chunkHash == lfhash
				|| chunkHash == rfhash || chunkHash == lbhash || chunkHash == rbhash;

	}

	void updateVisible(std::map<unsigned int, Chunk>& chunks)
	{
		sf::Vector3f cell = pos;
		cell /= CHUNK_SIZE;
		cell.x = (int)cell.x;
		cell.z = (int)cell.z;

		cell *= CHUNK_SIZE;

		std::map<unsigned int , Chunk>::iterator chIt;
		for(chIt = chunks.begin(); chIt != chunks.end(); chIt++)
		{
			chIt->second.visible = false;
			sf::Vector2f chunkPos = chIt->second.pos;
			sf::Vector3f chunkCell(chunkPos.x, cell.y, chunkPos.y);
			if(dist(cell, chunkCell) < RENDER_DIST*CHUNK_SIZE)
			{
				chIt->second.visible = true;

			}

		}



		changeVisible = false;

	}



	sf::SoundBuffer footStepBuffer;
	sf::Sound footStepSound;

	sf::Clock footStepTimer;

	bool localPlayer;
	sf::Vector3f rot;
	sf::Vector3f pos;
	sf::Vector3f target;

	AABB aabb;

	float walkSpeed;

	bool jumping;
	unsigned int jump;
	float jumpHeight;

	bool thirdPerson;

	unsigned int chash;
	unsigned int fhash;
	unsigned int bhash;
	unsigned int lhash;
	unsigned int rhash;
	unsigned int lfhash;
	unsigned int rfhash;
	unsigned int lbhash;
	unsigned int rbhash;

	bool changeVisible;

};

#endif /* HEDGEHOG_H_ */
