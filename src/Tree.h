#ifndef TREE_H_
#define TREE_H_

#include "Block.h"
#include "AABB.h"

bool canMakeTree(sf::Vector3f pos,
		std::vector<Block>& chunk)
{
	AABB trunkTest(sf::Vector3f(pos.x, pos.y+2.0, pos.z),
			sf::Vector3f(0.4, 2.0, 0.4));

	AABB leafTest(sf::Vector3f(pos.x, pos.y+3.0, pos.z),
			sf::Vector3f(1.5, 1.5, 1.5));

	for(unsigned int i = 0; i < chunk.size(); i++)
	{
		if(chunk[i].aabb.intersects(trunkTest))
		{
			return false;

		}
		else if(chunk[i].aabb.intersects(leafTest))
		{
			return false;

		}

	}

	return true;

}

void makeTree(sf::Vector3f pos,
		std::vector<Block>& chunk)
{
	if(!canMakeTree(pos, chunk))
		return;

	for(unsigned int i = 0; i < 4; i++)
	{
		chunk.push_back(Block::makeBlock(pos, Block::Log));
		pos.y += 1.0f;

	}

	for(unsigned int i = 0; i < 3; i++)
	{
		for(unsigned int j = 0; j < 3; j++)
		{
			for(unsigned int k = 0; k < 3; k++)
			{
				if(k == 1 && j == 1 && i != 0)
					continue;

				sf::Vector3f n(pos.x+1-k, pos.y, pos.z+1-j);
				chunk.push_back(Block::makeBlock(n, Block::Leaf));

			}

		}

		pos.y -= 1.0f;

	}

}



#endif /* TREE_H_ */
