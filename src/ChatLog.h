#ifndef CHATLOG_H_
#define CHATLOG_H_

#include <SFML/Graphics.hpp>

class ChatLog
{
public:
	ChatLog(int max,
			sf::Vector2f pos,
			sf::Font font) :
				max(max),
				pos(pos),
				font(font)
	{
	}

	void add(std::string str)
	{
		for(unsigned int i = 0; i < text.size(); i++)
		{
			sf::Color c = text[i].getColor();
			c.a -= 50;
			text[i].setColor(c);
			text[i].move(0, -14);

		}

		sf::Text t(str, font, 14);
		t.setColor(sf::Color::Black);
		t.setPosition(pos);
		text.push_back(t);

		if(text.size() > max)
			text.erase(text.begin());

	}

	void add(sf::Text t)
	{
		for(unsigned int i = 0; i < text.size(); i++)
		{
			sf::Color c = text[i].getColor();
			c.a -= 50;
			text[i].setColor(c);
			text[i].move(0, -14);

		}

		t.setPosition(pos);
		text.push_back(t);

		if(text.size() > max)
			text.erase(text.begin());

	}

	void draw(sf::RenderWindow& window)
	{
		for(unsigned int i = 0; i < text.size(); i++)
		{
			window.draw(text[i]);

		}

	}

	std::vector<sf::Text> text;
	int max;
	sf::Vector2f pos;
	sf::Font font;

};

#endif /* CHATLOG_H_ */
