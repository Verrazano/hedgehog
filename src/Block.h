#ifndef BLOCK_H_
#define BLOCK_H_

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "AABB.h"
#include "Util.h"

class Block
{
public:
	Vert v[24];

	float secondsToBreak;

	typedef enum Type
	{
		Grass = 0,
		Dirt,
		Stone,
		Brick,
		Wood,
		Log,
		Leaf,
		Coal,
		Iron

	} Type;

	typedef enum Blocking
	{
		None = 1 << 0,
		Left = 1 << 1,
		Right = 1 << 2,
		Top = 1 << 3,
		Bot = 1 << 4,
		Front = 1 << 5,
		Back = 1 << 6

	} Blocking;

	static Block makeBlock(sf::Vector3f pos,
			Type type)
	{
		//default stone
		sf::Vector2f top(260.0/1024.0, 650.0/2048.0);
		sf::Vector2f bot(260.0/1024.0, 650.0/2048.0);
		sf::Vector2f sides(260.0/1024.0, 650.0/2048.0);

		float secondsToBreak = 1;

		if(type == Grass)
		{
			top = sf::Vector2f(520.0/1024.0, 780.0/2048.0);
//			top = sf::Vector2f(520.0/1024.0, 1170.0/2048.0);
			bot = sf::Vector2f(650.0/1024.0, 130.0/2048.0);
			sides = sf::Vector2f(650.0/1024, 0.0);

			secondsToBreak = .75;
		}
		else if(type == Dirt)
		{
			top = sf::Vector2f(650.0/1024.0, 130.0/2048.0);
			bot = sides = top;

			secondsToBreak = .75;
		}
		else if(type == Brick)
		{
			top = sf::Vector2f(260.0/1024.0, 1560.0/2048.0);
			bot = sides = top;

			secondsToBreak = 2;
		}
		else if(type == Wood)
		{
			top = sf::Vector2f(0.0, 260.0/2048.0);
			bot = sides = top;

			secondsToBreak = 1.2;
		}
		else if(type == Log)
		{
			top = sf::Vector2f(0.0, 1300.0/2048.0);
			bot = top;
			sides = sf::Vector2f(0.0, 1430.0/2048.0);

			secondsToBreak = 1.2;
		}
		else if(type == Leaf)
		{
			top = sf::Vector2f(390/1024.0, 520.0/2048.0);
			bot = sides = top;

			secondsToBreak = .25;
		}
		else if(type == Coal)
		{
			sides = sf::Vector2f(260.0/1024.0, 260.0/2048.0);
			top = sf::Vector2f(260.0/1024, 130.0/2048.0);
			bot = top;

			secondsToBreak = 2.2;

		}

		Block b(pos, type, top, bot, sides, secondsToBreak);
		b.halfWidth = 0.501;

		return b;

	}

	Block(sf::Vector3f pos,
			Type type,
			sf::Vector2f top,
			sf::Vector2f bot,
			sf::Vector2f sides,
			float seconds = 1) :
		pos(pos),
		type(type),
		top(top),
		bot(bot),
		sides(sides)
	{
		resetBlocking();
		aabb = AABB(pos, sf::Vector3f(0.5, 0.5, 0.5));
		halfWidth = 0.5;

		secondsToBreak = seconds;
		offset = 0;

		visible = false;
		init();

	}

	void init()
	{
		sf::Vector2f size(128.0/1024.0, 128/2048.0);

		//front
		v[0].pos = sf::Vector3f(pos.x+halfWidth, pos.y+halfWidth, pos.z+halfWidth);
		v[1].pos = sf::Vector3f(pos.x+halfWidth, pos.y-halfWidth, pos.z+halfWidth);
		v[2].pos = sf::Vector3f(pos.x-halfWidth, pos.y-halfWidth, pos.z+halfWidth);
		v[3].pos = sf::Vector3f(pos.x-halfWidth, pos.y+halfWidth, pos.z+halfWidth);

		v[0].tex = sf::Vector2f(sides.x, sides.y);
		v[1].tex = sf::Vector2f(sides.x, sides.y+size.y);
		v[2].tex = sf::Vector2f(sides.x+size.x, sides.y+size.y);
		v[3].tex = sf::Vector2f(sides.x+size.x, sides.y);
		//v[0].tex = v[1].tex = v[2].tex = v[3].tex = sf::Vector2f(0.0, 0.0);

		v[0].n = sf::Vector3f(0, 0, 1.0f);
		v[1].n = sf::Vector3f(0, 0, 1.0f);
		v[2].n = sf::Vector3f(0, 0, 1.0f);
		v[3].n = sf::Vector3f(0, 0, 1.0f);

		//right
		v[4].pos = sf::Vector3f(pos.x+halfWidth, pos.y+halfWidth, pos.z+halfWidth);
		v[5].pos = sf::Vector3f(pos.x+halfWidth, pos.y-halfWidth, pos.z+halfWidth);
		v[6].pos = sf::Vector3f(pos.x+halfWidth, pos.y-halfWidth, pos.z-halfWidth);
		v[7].pos = sf::Vector3f(pos.x+halfWidth, pos.y+halfWidth, pos.z-halfWidth);

		v[4].tex = sf::Vector2f(sides.x, sides.y);
		v[5].tex = sf::Vector2f(sides.x, sides.y+size.y);
		v[6].tex = sf::Vector2f(sides.x+size.x, sides.y+size.y);
		v[7].tex = sf::Vector2f(sides.x+size.x, sides.y);

		v[4].n = sf::Vector3f(1.0f, 0, 0);
		v[5].n = sf::Vector3f(1.0f, 0, 0);
		v[6].n = sf::Vector3f(1.0f, 0, 0);
		v[7].n = sf::Vector3f(1.0f, 0, 0);

		//back
		v[8].pos = sf::Vector3f(pos.x+halfWidth, pos.y+halfWidth, pos.z-halfWidth);
		v[9].pos = sf::Vector3f(pos.x+halfWidth, pos.y-halfWidth, pos.z-halfWidth);
		v[10].pos = sf::Vector3f(pos.x-halfWidth, pos.y-halfWidth, pos.z-halfWidth);
		v[11].pos = sf::Vector3f(pos.x-halfWidth, pos.y+halfWidth, pos.z-halfWidth);

		v[8].tex = sf::Vector2f(sides.x, sides.y);
		v[9].tex = sf::Vector2f(sides.x, sides.y+size.y);
		v[10].tex = sf::Vector2f(sides.x+size.x, sides.y+size.y);
		v[11].tex = sf::Vector2f(sides.x+size.x, sides.y);

		v[8].n = sf::Vector3f(0, 0, -1.0f);
		v[9].n = sf::Vector3f(0, 0, -1.0f);
		v[10].n = sf::Vector3f(0, 0, -1.0f);
		v[11].n = sf::Vector3f(0, 0, -1.0f);

		//left
		v[12].pos = sf::Vector3f(pos.x-halfWidth, pos.y+halfWidth, pos.z+halfWidth);
		v[13].pos = sf::Vector3f(pos.x-halfWidth, pos.y-halfWidth, pos.z+halfWidth);
		v[14].pos = sf::Vector3f(pos.x-halfWidth, pos.y-halfWidth, pos.z-halfWidth);
		v[15].pos = sf::Vector3f(pos.x-halfWidth, pos.y+halfWidth, pos.z-halfWidth);

		v[12].tex = sf::Vector2f(sides.x, sides.y);
		v[13].tex = sf::Vector2f(sides.x, sides.y+size.y);
		v[14].tex = sf::Vector2f(sides.x+size.x, sides.y+size.y);
		v[15].tex = sf::Vector2f(sides.x+size.x, sides.y);

		v[12].n = sf::Vector3f(-1.0f, 0, 0);
		v[13].n = sf::Vector3f(-1.0f, 0, 0);
		v[14].n = sf::Vector3f(-1.0f, 0, 0);
		v[15].n = sf::Vector3f(-1.0f, 0, 0);

		//top
		v[16].pos = sf::Vector3f(pos.x+halfWidth, pos.y+halfWidth, pos.z+halfWidth);
		v[17].pos = sf::Vector3f(pos.x-halfWidth, pos.y+halfWidth, pos.z+halfWidth);
		v[18].pos = sf::Vector3f(pos.x-halfWidth, pos.y+halfWidth, pos.z-halfWidth);
		v[19].pos = sf::Vector3f(pos.x+halfWidth, pos.y+halfWidth, pos.z-halfWidth);

		v[16].tex = sf::Vector2f(top.x, top.y);
		v[17].tex = sf::Vector2f(top.x, top.y+size.y);
		v[18].tex = sf::Vector2f(top.x+size.x, top.y+size.y);
		v[19].tex = sf::Vector2f(top.x+size.x, top.y);

		v[16].n = sf::Vector3f(0, 1.0f, 0);
		v[17].n = sf::Vector3f(0, 1.0f, 0);
		v[18].n = sf::Vector3f(0, 1.0f, 0);
		v[19].n = sf::Vector3f(0, 1.0f, 0);

		//bot
		v[20].pos = sf::Vector3f(pos.x+halfWidth, pos.y-halfWidth, pos.z+halfWidth);
		v[21].pos = sf::Vector3f(pos.x-halfWidth, pos.y-halfWidth, pos.z+halfWidth);
		v[22].pos = sf::Vector3f(pos.x-halfWidth, pos.y-halfWidth, pos.z-halfWidth);
		v[23].pos = sf::Vector3f(pos.x+halfWidth, pos.y-halfWidth, pos.z-halfWidth);

		v[20].tex = sf::Vector2f(bot.x, bot.y);
		v[21].tex = sf::Vector2f(bot.x, bot.y+size.y);
		v[22].tex = sf::Vector2f(bot.x+size.x, bot.y+size.y);
		v[23].tex = sf::Vector2f(bot.x+size.x, bot.y);

		v[20].n = sf::Vector3f(0, -1.0f, 0);
		v[21].n = sf::Vector3f(0, -1.0f, 0);
		v[22].n = sf::Vector3f(0, -1.0f, 0);
		v[23].n = sf::Vector3f(0, -1.0f, 0);


	}

	void draw()
	{
		sf::Vector2f size(128.0/1024.0, 128/2048.0);

		glColor3f(1.0f -offset, 1.0f -offset, 1.0f - offset);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		char* start = (char*)&v;
		glVertexPointer(3, GL_FLOAT, sizeof(Vert), start + offsetof(Vert, pos));
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vert), start + offsetof(Vert, tex));

		glDrawArrays(GL_QUADS, 0, 24);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	}

	void resetBlocking()
	{
		blocking = None;

	}

	bool isBlocking(Blocking b)
	{
		return blocking&b;

	}

	void addBlocking(sf::Vector3f p)
	{
		sf::Vector3f d = p - pos;

		if(d.x < 0)
			blocking |= Left;
		else if(d.x > 0)
			blocking |= Right;
		else if(d.y > 0)
			blocking |= Top;
		else if(d.y < 0)
			blocking |= Bot;
		else if(d.z > 0)
			blocking |= Front;
		else if(d.z < 0)
			blocking |= Back;

	}

	void getVertsNotBlocked(std::vector<Vert>& verts)
	{
		if(!isBlocking(Front))
		{
			verts.push_back(v[0]);
			verts.push_back(v[1]);
			verts.push_back(v[2]);
			verts.push_back(v[3]);

		}

		if(!isBlocking(Right))
		{
			verts.push_back(v[4]);
			verts.push_back(v[5]);
			verts.push_back(v[6]);
			verts.push_back(v[7]);

		}

		if(!isBlocking(Back))
		{
			verts.push_back(v[8]);
			verts.push_back(v[9]);
			verts.push_back(v[10]);
			verts.push_back(v[11]);

		}

		if(!isBlocking(Left))
		{
			verts.push_back(v[12]);
			verts.push_back(v[13]);
			verts.push_back(v[14]);
			verts.push_back(v[15]);

		}

		if(!isBlocking(Top))
		{
			verts.push_back(v[16]);
			verts.push_back(v[17]);
			verts.push_back(v[18]);
			verts.push_back(v[19]);

		}

		if(!isBlocking(Bot))
		{
			verts.push_back(v[20]);
			verts.push_back(v[21]);
			verts.push_back(v[22]);
			verts.push_back(v[23]);

		}

	}


	AABB aabb;

	sf::Vector3f pos;

	Type type;

	sf::Vector2f top;
	sf::Vector2f bot;
	float halfWidth, offset; //Offset determines the color tint of the block.
	sf::Vector2f sides;
	bool visible;
	unsigned int blocking;

};

#endif /* BLOCK_H_ */
