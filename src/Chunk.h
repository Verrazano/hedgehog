#ifndef CHUNK_H_
#define CHUNK_H_

#include "Util.h"
#include "Block.h"
#include "Perlin/Perlin.h"
#include "Tree.h"
#include <algorithm>

bool sortBlocks(Block a, Block b)
{
	return a.visible > b.visible;

}

class Chunk
{
public:
	Chunk()
	{
		//fuck you

	}

	Chunk(sf::Vector3f lightPos,
			unsigned int hash,
			sf::Vector2f pos,
			Perlin& noise,
			unsigned int size = CHUNK_SIZE,
			unsigned int height = 6,
			unsigned int maxHeight = 10) :
				pos(pos),
				hash(hash)
	{
		shadowsComputed = false;
		shadowPointer = 0;
//		genStandard(noise, size, height, maxHeight);
		genSphere(noise, size, height, maxHeight+8, size/1.85f);
		spawnOres();
		recomputeTerrarian(lightPos);

	}

	void genSphere(Perlin& noise,
			unsigned int size,
			unsigned int height,
			unsigned int maxHeight,
			float radius)
	{
		int pick = rand()%100;

		if(pick <= 25
				|| (pos.x == WORLD_SIZE*CHUNK_SIZE/2.0f
				&& pos.y == WORLD_SIZE*CHUNK_SIZE/2.0f))
		{
			sf::Vector3f center(pos.x + (float)size/2.0f, (float)maxHeight/2.0f, pos.y + (float)size/2.0f);
			for(unsigned int i = 0; i < maxHeight; i++)
			{
				for(unsigned int j = 0; j < size; j++)
				{
					for(unsigned int k = 0; k < size; k++)
					{
						sf::Vector3f blockPos(j+pos.x, i, k+pos.y);
						if(dist(center, blockPos) <= radius)
						{
							sf::Vector3f d = blockPos - center;
							if(d.y < 2)
							{
								data.push_back(Block::makeBlock(sf::Vector3f(j+pos.x, i, k+pos.y), Block::Dirt));

							}

						}

					}

				}

			}

			std::vector<sf::Vector3f> placePos;
			for(unsigned int i = 0; i < data.size(); i++)
			{
				if(data[i].type == Block::Dirt)
				{
					sf::Vector3f p =  data[i].pos;
					bool foundAbove = false;
					for(unsigned int j = 0; j < data.size(); j++)
					{
						sf::Vector3f p2 = data[j].pos;
						if(p.x == p2.x && p.z == p2.z && p.y == p2.y-1)
						{
							foundAbove = true;
							break;

						}

					}

					if(!foundAbove)
					{
						p.y +=1;
						placePos.push_back(p);

					}

				}

			}

			float rat = CHUNK_SIZE*WORLD_SIZE;
			for(unsigned int i = 0; i < placePos.size(); i++)
			{
				float xrat = (placePos[i].x)/rat;
				float yrat = (placePos[i].z)/rat;
				int heightOffset = noise.Get(xrat, yrat)*13;
				unsigned int tHeight = (unsigned int)fmax((int)center.y-1+heightOffset, center.y-3);
				tHeight += 1;
				tHeight = fmin(tHeight, maxHeight);

				if(tHeight > placePos[i].y)
				{
					for(unsigned int j = (unsigned int)placePos[i].y; j < tHeight; j++)
					{
						data.push_back(Block::makeBlock(sf::Vector3f(placePos[i].x, j, placePos[i].z), Block::Dirt));

					}

				}
				else
				{
					std::vector<Block>::iterator it;
					for(it = data.begin(); it != data.end();)
					{
						Block& b = (*it);
						if(b.pos.x == placePos[i].x
								&& b.pos.z == placePos[i].z
								&& b.pos.y > tHeight)
						{
							it = data.erase(it);
							continue;

						}

						it++;

					}

				}

			}

			for(unsigned int i = 0; i < data.size(); i++)
			{
				if(data[i].type == Block::Dirt)
				{
					sf::Vector3f p =  data[i].pos;
					bool foundAbove = false;
					for(unsigned int j = 0; j < data.size(); j++)
					{
						sf::Vector3f p2 = data[j].pos;
						if(p.x == p2.x && p.z == p2.z && p.y == p2.y-1)
						{
							foundAbove = true;
							break;

						}

					}

					if(!foundAbove)
					{
						data[i] = Block::makeBlock(sf::Vector3f(p), Block::Grass);
						p.y += 1;
						int r = rand()%100;
						if(r < 1)
						{
							makeTree(p, data);

						}
						else if(r > 5 && r < 7)
						{
							data.push_back(Block::makeBlock(p, Block::Brick));

						}

					}

				}

			}

			for(unsigned int i = 0; i < data.size(); i++)
			{
				Block& b = data[i];
				sf::Vector3f bpos = b.pos;

				int blockingCount = 0;

				for(unsigned int j = 0; j < data.size(); j++)
				{
					if(i != j)
					{
						Block::Type type = data[j].type;
						bool ok = (type == Block::Dirt || type == Block::Stone);
						if(ok && quickDist(bpos, data[j].pos) <= 1.1f)
						{
							blockingCount++;
							if(blockingCount >= 6)
							{
								data[i] = Block::makeBlock(sf::Vector3f(bpos), Block::Stone);
								break;

							}

						}

					}

				}

			}

		}

	}

	void genStandard(Perlin& noise,
			unsigned int size,
			unsigned int height,
			unsigned int maxHeight,
			bool tree = true,
			bool brick = true)
	{
		float rat = CHUNK_SIZE*WORLD_SIZE;
		for(unsigned int j = 0; j < size; j++)
		{
			for(unsigned int k = 0; k < size; k++)
			{
				float xrat = (j+pos.x)/rat;
				float yrat = (k+pos.y)/rat;
				int heightOffset = noise.Get(xrat, yrat)*maxHeight;
				unsigned int tHeight = (unsigned int)fmax((int)height+heightOffset, 2);
				tHeight = fmin(tHeight, maxHeight);
				//std::cout << "tHeight: " << tHeight << "\n";
				for(unsigned int i = 0; i < tHeight; i++)
				{
					Block::Type type = Block::Stone;
					if(i == tHeight-1)
						type = Block::Grass;
					else if(i >= tHeight - (height/3))
						type = Block::Dirt;


					data.push_back(Block::makeBlock(sf::Vector3f(j+pos.x, i, k+pos.y), type));

					if(i == tHeight-1)
					{
						int r = rand()%100;
						if(r < 1)
						{
							makeTree(sf::Vector3f(j+pos.x, i+1, k+pos.y), data);

						}
						else if(r > 5 && r < 7)
						{
							data.push_back(Block::makeBlock(sf::Vector3f(j+pos.x, i+1, k+pos.y), Block::Brick));

						}

					}

				}


			}

		}

	}

	void spawnOres()
	{
		for(unsigned int i = 0; i < data.size(); i++)
		{
			if(data[i].type == Block::Stone)
			{
				unsigned int offset = 0;
				for(unsigned int j = 0; j < data.size(); j++)
				{
					if(data[j].type == Block::Coal
							&& quickDist(data[j].pos, data[i].pos) <= 1.1)
					{
						offset += 6;

					}

				}

				if(rand()%100 < 3 + offset)
				{
					data[i] = Block::makeBlock(sf::Vector3f(data[i].pos), Block::Coal);

				}

			}

		}

	}

	void drawDepth()
	{
		glEnableClientState(GL_VERTEX_ARRAY);

		glColor3f(1.0f, 1.0f, 1.0f);

		char* start = (char*)&verts[0];
		glVertexPointer(3, GL_FLOAT, sizeof(Vert), start + offsetof(Vert, pos));

		glDrawArrays(GL_QUADS, 0, verts.size());

		glDisableClientState(GL_VERTEX_ARRAY);

	}

	void draw()
	{
		if(verts.size() > 0)
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glEnableClientState(GL_NORMAL_ARRAY);

			glColor3f(1.0f, 1.0f, 1.0f);
			char* start = (char*)&verts[0];
			glVertexPointer(3, GL_FLOAT, sizeof(Vert), start + offsetof(Vert, pos));
			glTexCoordPointer(2, GL_FLOAT, sizeof(Vert), start + offsetof(Vert, tex));
			glNormalPointer(GL_FLOAT, sizeof(Vert), start + offsetof(Vert, n));

			glDrawArrays(GL_QUADS, 0, verts.size());

			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			glDisableClientState(GL_NORMAL_ARRAY);

		}

	}

	void recomputeTerrarian(sf::Vector3f lightPos)
	{
		for(unsigned int i = 0; i < data.size(); i++)
		{
			Block& b = data[i];
			sf::Vector3f bpos = b.pos;

			b.resetBlocking();
			int blockingCount = 0;

			for(unsigned int j = 0; j < data.size(); j++)
			{
				if(i != j)
				{
					Block::Type type = data[j].type;
					bool ok = (type == Block::Leaf || b.type == Block::Leaf);
					if(!ok && quickDist(bpos, data[j].pos) <= 1.1f)
					{
						b.addBlocking(data[j].pos);
						blockingCount++;
						if(blockingCount >= 6)
							break;

					}

				}

			}

			b.visible = blockingCount < 6;

		}

		std::stable_sort(data.begin(), data.end(), sortBlocks);

		verts.clear();
		verts.reserve(16*16*24*16);
		for(unsigned int i = 0; i < data.size(); i++)
		{
			if(data[i].visible)
				data[i].getVertsNotBlocked(verts);
			else
				break;

		}

		recomputeShadows(lightPos);

	}

	void recomputeTerrarian(sf::Vector3f changed,
			sf::Vector3f lightPos)
	{
		int updateCount;
		for(unsigned int i = 0; i < data.size(); i++)
		{
			Block& b = data[i];
			sf::Vector3f bpos = b.pos;

			if(quickDist(bpos, changed) <= 1.1f)
			{
				b.resetBlocking();
				int blockingCount = 0;

				for(unsigned int j = 0; j < data.size(); j++)
				{
					if(i != j)
					{
						Block::Type type = data[j].type;
						bool ok = (type == Block::Leaf || b.type == Block::Leaf);
						if(!ok && quickDist(bpos, data[j].pos) <= 1.1f)
						{
							data[i].addBlocking(data[j].pos);
							blockingCount++;
							if(blockingCount >= 6)
								break;

						}

					}

				}

				updateCount++;
				b.visible = blockingCount < 6;

			}

			if(updateCount == 6)
			{
				break;

			}

		}

		std::stable_sort(data.begin(), data.end(), sortBlocks);

		verts.clear();
		verts.reserve(16*16*24*16);
		for(unsigned int i = 0; i < data.size(); i++)
		{
			if(data[i].visible)
				data[i].getVertsNotBlocked(verts);
			else
				break;

		}

		recomputeShadows(lightPos);

	}

	void recomputeShadows(sf::Vector3f lightPos)
	{
		if(shadowsComputed)
			glDeleteLists(shadowPointer, 1);

		shadowPointer = glGenLists(1);

		glNewList(shadowPointer, GL_COMPILE);
		for(unsigned int i = 0; i < verts.size(); i+=4)
		{
			renderShadowVolume(&verts[i], lightPos);

		}
		glEndList();

		shadowsComputed = true;

	}

	void drawShadows()
	{
		//glCallList(shadowPointer);


		glColor3f(0.2, 0.2, 0.2);
		glCallList(shadowPointer);

	}

	std::vector<Vert> verts;
	sf::Vector2f pos;
	std::vector<Block> data;
	bool visible;
	unsigned int hash;

	GLuint shadowPointer;
	bool shadowsComputed;

};

#endif /* CHUNK_H_ */
