#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Network.hpp>
#include <SFML/Audio.hpp>

#include "Block.h"
#include <math.h>
#include <iostream>
#include "Skybox.h"
#include "Util.h"
#include "Hedgehog.h"
#include "Tree.h"
#include <algorithm>
#include "Item.h"
#include "ChatLog.h"
#include "Inventory.h"
#include "Perlin/Perlin.h"
#include "Chunk.h"
#include <map>

sf::Vector3f g_playerPos;

bool sortAABBs(AABB i, AABB j)
{
	float di = dist(i.c, g_playerPos);
	float dj = dist(j.c, g_playerPos);
	return di < dj;
}

int main(int argc, char** argv)
{
	Perlin noise(6, 2, 1, time(NULL));
	srand(time(NULL));

    sf::ContextSettings settings;
    settings.depthBits = 32;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;
    settings.majorVersion = 3;
    settings.minorVersion = 0;

	sf::RenderWindow window(sf::VideoMode(854, 480),
			"hedgehog simulator", sf::Style::Close, settings);
	window.setFramerateLimit(30);
	//window.setMouseCursorVisible(false);

	/*sf::SocketSelector selector;
	sf::TcpListener listener;
	listener.listen(50301);
	selector.add(listener);

	sf::TcpSocket server;*/

	/*sf::TcpListener listener;
	sf::TcpSocket socket;

	std::string choice;
	std::cout << "are you the host? [yes/no]: ";
	std::cin >> choice;
	if(choice == "yes")
	{
		unsigned int port;
		std::cout << "port: ";
		std::cin >> port;

		std::cout << "waiting for other player to join\n";
		listener.setBlocking(true);
		sf::Socket::Status status = listener.listen(port);
		if(status == sf::Socket::Done)
		{
			listener.accept(socket);

		}
		else
		{
			std::cout << "client failed to connect\n";
			return 0;

		}

	}
	else
	{
		std::string ip;
		unsigned int port;
		std::cout << "remote host: ";
		std::cin >> ip;
		std::cout << "port: ";
		std::cin >> port;

		sf::IpAddress ipAddr(ip);
		sf::Socket::Status status = socket.connect(ipAddr, port, sf::seconds(5));

		if(status != sf::Socket::Done)
		{
			std::cout << "failed to connect to server\n";
			return 0;

		}

	}

	socket.setBlocking(false);

	unsigned int frame = 0;*/

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.7, 0.9, 1.0, 1.0);

	//glAlphaFunc(GL_GREATER, 0.4);
	//glEnable(GL_ALPHA_TEST);

	//glEnable(GL_BLEND);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	GLfloat ambientColor[] = {0.75f, 0.75f, 0.75f, 1.0f};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);


    GLfloat mat_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat mat_shininess[] = {50.0f};
//    GLfloat mat_color[] = {1.0f, 1.0f, 1.0f, 1.0f};

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);
//    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_color);

//    glEnable(GL_FOG);
	glFogi(GL_FOG_MODE, GL_LINEAR);
//	glFogf(GL_FOG_START, 20.0f);
	glFogf(GL_FOG_END, 47.0f);

	GLfloat fog_density = 0.25;
	GLfloat fog_color[4] = {0.5f, 0.0f, 0.5f, 1.0f};

	glFogf(GL_FOG_DENSITY, fog_density);
	glFogfv(GL_FOG_COLOR, fog_color);

	glHint (GL_FOG_HINT, GL_NICEST);

	sf::Font font;
	font.loadFromFile("supermario.ttf");

	sf::Image world;
	world.loadFromFile("world.png");

	GLuint worldTex;
	glGenTextures(1, &worldTex);
	glBindTexture(GL_TEXTURE_2D, worldTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
			world.getSize().x, world.getSize().y,
			0, GL_RGBA, GL_UNSIGNED_BYTE, world.getPixelsPtr());


	sf::Image itemIMG;
	itemIMG.loadFromFile("spritesheet_items.png");

	GLuint itemTex;
	glGenTextures(1, &itemTex);
	glBindTexture(GL_TEXTURE_2D, itemTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
			itemIMG.getSize().x, itemIMG.getSize().y,
			0, GL_RGBA, GL_UNSIGNED_BYTE, itemIMG.getPixelsPtr());

	sf::Image hedgehog;
	hedgehog.loadFromFile("hedgehog.png");

	GLuint hedgehogTex;
	glGenTextures(1, &hedgehogTex);
	glBindTexture(GL_TEXTURE_2D, hedgehogTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
			hedgehog.getSize().x, hedgehog.getSize().y,
			0, GL_RGBA, GL_UNSIGNED_BYTE, hedgehog.getPixelsPtr());

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, 854.0f/480.0f, 0.2f, 200.0f);

	sf::Vector3f spawn;
	sf::Vector3f camRot(0.0f, 0.0f, 0.0f);

	unsigned int height = 6;
//	unsigned maxHeight =  20;

	Inventory playerInventory(sf::Vector2u(3, 3), font);

	Skybox skybox;

	std::vector<Item> items;

	sf::RectangleShape loading;
	loading.setFillColor(sf::Color::Green);
	loading.setPosition(854/2, 480/2);

	sf::Text loadingText("Building World", font);
	loadingText.setOrigin(loadingText.getLocalBounds().width/2, 0);
	loadingText.setPosition(854/2, 480/2 - 75);

	float loaded = 0.0f;
	float max = WORLD_SIZE*WORLD_SIZE;

	spawn = sf::Vector3f((float)WORLD_SIZE*CHUNK_SIZE/2.0f + CHUNK_SIZE/2.0f, height+10, (float)WORLD_SIZE*CHUNK_SIZE/2.0f + CHUNK_SIZE/2.0f);

	GLfloat light_pos[] = {0.0, 1.0f, -1.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	sf::Vector3f lightPos(WORLD_SIZE*CHUNK_SIZE, spawn.y, WORLD_SIZE*CHUNK_SIZE);
	lightPos.y += 6.0f;

	std::map<unsigned int, Chunk> chunks;

//	glDisable(GL_FOG);
	window.pushGLStates();

	for(unsigned int j = 0; j < WORLD_SIZE; j++)
	{
		for(unsigned int k = 0; k < WORLD_SIZE; k++)
		{
			unsigned int hash = computeHash(sf::Vector2f(j*CHUNK_SIZE, k*CHUNK_SIZE));
			chunks[hash] = Chunk(lightPos, hash, sf::Vector2f(j*CHUNK_SIZE, k*CHUNK_SIZE), noise);
			loaded++;

			float loadRatio = loaded/max;
			loading.setSize(sf::Vector2f(200*loadRatio, 50));
			loading.setOrigin(100*loadRatio, 25);

			window.clear(sf::Color::Black);
			window.draw(loading);
			window.draw(loadingText);
			window.display();
			/*int heightOffset = noise.Get((float)j/(float)size, (float)k/(float)size)*10.0f;
			unsigned int tHeight = (unsigned int)fmax(height+heightOffset, 2);
			tHeight = fmin(tHeight, maxHeight);
			//std::cout << "tHeight: " << tHeight << "\n";
			if(j+1 == size/2 && k+1 == size/2)
			{


			}
			for(unsigned int i = 0; i < tHeight; i++)
			{
				Block::Type type = Block::Stone;
				if(i == tHeight-1)
					type = Block::Grass;
				else if(i >= tHeight - (height/3))
					type = Block::Dirt;

				chunk.push_back(Block::makeBlock(sf::Vector3f(j, i, k), type));

				if(i == tHeight-1 && rand()%18 == 0)
				{
	//					chunk.push_back(Block::makeBlock(sf::Vector3f(j, i+1, k), Block::Brick));
					makeTree(sf::Vector3f(j, i+1, k), chunk);

				}

			}*/


		}

	}

	window.popGLStates();
//	glEnable(GL_FOG);

	Hedgehog remotePlayer(spawn);
	Hedgehog player(spawn);
	player.localPlayer = true;
	//player.thirdPerson = true;

	bool paused = true;
	bool focus = true;
	bool invOpen = false;
//	bool client = false;

	sf::RectangleShape up(sf::Vector2f(1, 6));
	up.setOrigin(0.5, 3);
	up.setPosition(854/2, 480/2);
	up.setFillColor(sf::Color::Black);

	sf::RectangleShape across(sf::Vector2f(6, 1));
	across.setOrigin(3, 0.5);
	across.setPosition(854/2, 480/2);
	across.setFillColor(sf::Color::Black);

	sf::Text pausedText("          Paused\n"
						"(press escape to resume)", font);
	pausedText.setColor(sf::Color::Black);
	pausedText.setOrigin(pausedText.getLocalBounds().width/2, pausedText.getLocalBounds().height/2);
	pausedText.setPosition(854/2, 240);

	ChatLog chatlog(5, sf::Vector2f(14, 480-28), font);
//	chatlog.add("global: " + sf::IpAddress::getPublicAddress().toString());
//	chatlog.add("local: " + sf::IpAddress::getLocalAddress().toString());

	sf::Clock pressTimer;
	sf::Clock frameTimer;
	float frameRate = 0.0f;

	while(window.isOpen())
	{
		/*{
			sf::Packet packet;
			sf::Socket::Status status = socket.receive(packet);
			if(status == sf::Socket::Disconnected)
			{
				std::cout << "disconnected\n";
				return 0;

			}
			else
			{
				packet >> target.x;
				packet >> target.y;
				packet >> target.z;
				packet >> remoteRot.x;
				packet >> remoteRot.y;

			}

		}*/

		bool rightClicked = false;
		bool leftClicked = false;
		bool qPressed = false;

		sf::Event event;
		while(window.pollEvent(event))
		{
			if(!paused)
			{
				playerInventory.update(event, invOpen);

			}

			if(event.type == sf::Event::Closed)
			{
				window.close();
				return 0;

			}
			else if(focus && event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
			{
				paused = !paused;
				window.setMouseCursorVisible(paused);

			}
            else if(!paused && !invOpen && focus && event.type == sf::Event::MouseMoved)
            {
            	float delta = 1.0f/30.0f;
            	float lookSpeed = 1.2f;
            	float x = -lookSpeed*delta*(854.0/2.0 - event.mouseMove.x);
            	float y = -lookSpeed*delta*(480.0/2.0 - event.mouseMove.y);
            	camRot.x += y;
            	camRot.y += x;

            	camRot.x = fmax(fmin(camRot.x, 90), -90);

            }
            else if(event.type == sf::Event::GainedFocus)
            	focus = true;
            else if(event.type == sf::Event::LostFocus)
            {
            	focus = false;
            	if(rightClicked)
            	{
            		pressTimer.restart();
            		rightClicked = false;
            	}

            }
            else if(event.type == sf::Event::MouseButtonReleased)
            {
            	if(rightClicked)
            	{
            		pressTimer.restart();
            		rightClicked = false;
            	}
            }
            else if(!paused && event.type == sf::Event::MouseButtonPressed)
            {
            	if(event.mouseButton.button == sf::Mouse::Right)
            		leftClicked = true;
            	else if(event.mouseButton.button == sf::Mouse::Left)
            	{
            		pressTimer.restart();
            	}

            }

			/////KEY PRESSES GO HERE
			if(event.type == sf::Event::KeyPressed)
			{
				sf::Keyboard::Key key = event.key.code;
				if(key == sf::Keyboard::Q)
				{
					qPressed = true;

				}
				else if(key == sf::Keyboard::E)
				{
					invOpen = !invOpen;
					window.setMouseCursorVisible(invOpen);

				}
				/*else if(key == sf::Keyboard::J)
				{
					server.disconnect();
					std::string ip;
					std::cout << "remote host ip: ";
					std::cin >> ip;

					sf::Socket::Status status = server.connect(sf::IpAddress(ip), 50301, sf::seconds(1.0));
					if(status == sf::Socket::Done)
					{
						client = true;

					}

				}*/

			}

			if(!paused && sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				rightClicked = true;
			}

		}

		if(!paused && !invOpen)
		{
			sf::Mouse::setPosition(sf::Vector2i(854/2, 240), window);

		}

		player.update(focus, paused || invOpen, camRot);
		g_playerPos = player.pos;

		if(player.pos.y < -5)
		{
			player.pos = spawn;
			player.aabb.c = spawn;

		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glPushMatrix();
		glRotatef(camRot.x, 1.0f, 0.0f, 0.0f);
		glRotatef(camRot.y, 0.0f, 1.0f, 0.0f);
		glRotatef(camRot.z, 0.0f, 0.0f, 1.0f);

		glEnable(GL_TEXTURE_2D);

//		glDisable(GL_FOG);
		//glDisable(GL_LIGHTING);
		skybox.draw();
		//glEnable(GL_LIGHTING);
//		glEnable(GL_FOG);

		sf::Vector3f camPos = player.getCamPos(camRot);
		glTranslatef(camPos.x, camPos.y, camPos.z);

		glBindTexture(GL_TEXTURE_2D, worldTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		int selectedChunk = 0;
		int selectedIndex = -1;

		float selectedDist = 10000.0f;
		sf::Vector3f selected;
		sf::Vector3f selector;
		bool foundPos = false;

		sf::Vector3f testStep = getViewAngle(camRot);
		sf::Vector3f lookPos(player.pos);
		lookPos.y += 0.5;

		if(qPressed)
		{
			Inventory::ItemData data = playerInventory.getSelected();
			if(data.amount != 0.0)
			{
				playerInventory.take(data.name);
				Item newIt = data.copyme;
				newIt.lifeTimer.restart();
				newIt.pos = lookPos;
				newIt.pos.y += 0.3;
				newIt.vel = testStep*0.5f;
				newIt.hash = computeHash(newIt.pos);
				items.push_back(newIt);

			}

		}

		sf::Vector3f aabbSize(0.2, 0.2, 0.2);
		AABB testAABB0(lookPos + testStep, aabbSize);
		AABB testAABB1(lookPos + testStep + testStep, aabbSize);
		AABB testAABB2(lookPos + testStep + testStep + testStep, aabbSize);
		AABB testAABB3(lookPos + testStep + testStep + testStep + testStep, aabbSize);

		std::vector<AABB> blocking;


		if(player.changeVisible)
			player.updateVisible(chunks);

		std::map<unsigned int, Chunk>::iterator chIt;
		for(chIt = chunks.begin(); chIt != chunks.end(); chIt++)
		{
			Chunk& ch = chIt->second;
			unsigned int i = chIt->first;

			if(ch.visible)
			{
				ch.draw();
				if(player.checkCollision(i))
				{
					for(unsigned int j = 0; j < ch.data.size(); j++)
					{
						ch.data[j].offset = 0;
						if(ch.data[j].visible)
						{
							if(player.aabb.intersects(ch.data[j].aabb))
							{
								blocking.push_back(ch.data[j].aabb);

							}

							if(ch.data[j].aabb.intersects(testAABB0)
									|| ch.data[j].aabb.intersects(testAABB1)
									|| ch.data[j].aabb.intersects(testAABB2)
									|| ch.data[j].aabb.intersects(testAABB3))
							{
								float d = dist(ch.data[j].aabb.c, lookPos);
								if(d < selectedDist)
								{
									selectedChunk = i;
									selectedDist = d;
									selected = ch.data[j].aabb.c;
									foundPos = true;
									selectedIndex = j;

								}

							}

						}
						else
						{
							break;

						}

					}

				}

			}

		}

		std::sort(blocking.begin(), blocking.end(), sortAABBs);
		for(unsigned int i = 0; i < blocking.size(); i++)
		{
			AABB intersection;
			if(player.aabb.intersects(blocking[i], intersection))
			{
				player.resolve(intersection);

			}

		}

		std::vector<Item>::iterator itIt;
		for(itIt = items.begin(); itIt != items.end();)
		{
			Item& item = (*itIt);
			if(item.lifeTimer.getElapsedTime().asSeconds() > 20.0
					|| item.pos.y < -0.5)
			{
				itIt = items.erase(itIt);
				continue;

			}

			if(player.aabb.intersects(item.aabb))
			{
				if(playerInventory.add(item))
				{
					itIt = items.erase(itIt);
					continue;

				}

			}

			Chunk& c = chunks[item.hash];
			for(unsigned int j = 0; j < c.data.size(); j++)
			{
				AABB inter;
				if(c.data[j].visible)
				{
					if(item.aabb.intersects(c.data[j].aabb, inter))
					{
						item.resolve(inter);

					}

				}
				else
				{
					break;

				}

			}


			item.update();
			item.draw();

			itIt++;

		}

		glBindTexture(GL_TEXTURE_2D, hedgehogTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		//player.draw();

		remotePlayer.update(focus, paused || invOpen, camRot);
		remotePlayer.draw();

		glDisable(GL_TEXTURE_2D);

		if(foundPos)
		{
			glColor4f(0.5f, 0.5f, 0.5f, 0.5f);
//			mat_color[0] = mat_color[1] = mat_color[2] = mat_color[3] = 0.5f;
//			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_color);
			drawCube(selected, 0.502);
//			mat_color[0] = mat_color[1] = mat_color[2] = mat_color[3] = 1.0f;
//			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_color);

			Plane frontFace(sf::Vector3f(selected.x, selected.y, selected.z+0.5),
					sf::Vector3f(0.0, 0.0, 1.0));

			Plane rightFace(sf::Vector3f(selected.x+0.5, selected.y, selected.z),
								sf::Vector3f(1.0, 0.0, 0.0));

			Plane leftFace(sf::Vector3f(selected.x-0.5, selected.y, selected.z),
								sf::Vector3f(-1.0, 0.0, 0.0));

			Plane backFace(sf::Vector3f(selected.x, selected.y, selected.z-0.5),
								sf::Vector3f(0.0, 0.0, -1.0));

			Plane topFace(sf::Vector3f(selected.x, selected.y+0.5, selected.z),
								sf::Vector3f(0.0, 1.0, 0.0));

			Plane botFace(sf::Vector3f(selected.x, selected.y-0.5, selected.z),
								sf::Vector3f(0.0, -1.0, 0.0));

			sf::Vector3f offset;

			Chunk& chunk = chunks[selectedChunk];

			bool clipped = false;

			if(frontFace.signedDist(lookPos) > 0)
			{
				sf::Vector3f p = frontFace.intersect(lookPos, lookPos + testStep*4.0f);
				AABB r(p, sf::Vector3f(0.01, 0.01, 0.01));

				if(chunk.data[selectedIndex].aabb.intersects(r))
				{
					glColor4f(1.0, 0.0, 0.0, 0.5);
					drawCube(r.c, 0.01);
					offset = sf::Vector3f(0.0, 0.0, 1.0);
					clipped = true;

				}

			}

			if(rightFace.signedDist(lookPos) > 0)
			{
				sf::Vector3f p = rightFace.intersect(lookPos, lookPos + testStep*4.0f);
				AABB r(p, sf::Vector3f(0.01, 0.01, 0.01));
				if(chunk.data[selectedIndex].aabb.intersects(r))
				{
					glColor4f(1.0, 0.0, 0.0, 0.5);
					drawCube(r.c, 0.01);
					offset = sf::Vector3f(1.0, 0.0, 0.0);
					clipped = true;

				}

			}

			if(leftFace.signedDist(lookPos) > 0)
			{
				sf::Vector3f p = leftFace.intersect(lookPos, lookPos + testStep*4.0f);
				AABB r(p, sf::Vector3f(0.01, 0.01, 0.01));
				if(chunk.data[selectedIndex].aabb.intersects(r))
				{
					glColor4f(1.0, 0.0, 0.0, 0.5);
					drawCube(r.c, 0.01);
					offset = sf::Vector3f(-1.0, 0.0, 0.0);
					clipped = true;

				}

			}

			if(backFace.signedDist(lookPos) > 0)
			{
				sf::Vector3f p = backFace.intersect(lookPos, lookPos + testStep*4.0f);
				AABB r(p, sf::Vector3f(0.01, 0.01, 0.01));
				if(chunk.data[selectedIndex].aabb.intersects(r))
				{
					glColor4f(1.0, 0.0, 0.0, 0.5);
					drawCube(r.c, 0.01);
					offset = sf::Vector3f(0.0, 0.0, -1.0);
					clipped = true;

				}

			}

			if(topFace.signedDist(lookPos) > 0)
			{
				sf::Vector3f p = topFace.intersect(lookPos, lookPos + testStep*4.0f);
				AABB r(p, sf::Vector3f(0.01, 0.01, 0.01));
				if(chunk.data[selectedIndex].aabb.intersects(r))
				{
					glColor4f(1.0, 0.0, 0.0, 0.5);
					drawCube(r.c, 0.01);
					offset = sf::Vector3f(0.0, 1.0, 0.0);
					clipped = true;

				}

			}

			if(botFace.signedDist(lookPos) > 0)
			{
				sf::Vector3f p = botFace.intersect(lookPos, lookPos + testStep*4.0f);
				AABB r(p, sf::Vector3f(0.01, 0.01, 0.01));
				if(chunk.data[selectedIndex].aabb.intersects(r))
				{
					glColor4f(1.0, 0.0, 0.0, 0.5);
					drawCube(r.c, 0.01);
					offset = sf::Vector3f(0.0, -1.0, 0.0);
					clipped = true;

				}

			}

			if(rightClicked)
			{
				if(pressTimer.getElapsedTime().asSeconds() > chunk.data[selectedIndex].secondsToBreak)
				{
					std::vector<Item> temp = Item::makeBlockItem(itemTex, worldTex, chunk.data[selectedIndex].type, selected);
					for(unsigned int i = 0; i < temp.size(); i++)
						items.push_back(temp[i]);
					//std::cout << "erase: " << selectedIndex << " of " << chunk.data.size() << "\n";
					chunk.data.erase(chunk.data.begin()+selectedIndex);
					chunk.recomputeTerrarian(selected, lightPos);

					pressTimer.restart();
				}
				else
				{
					float tempMod = pressTimer.getElapsedTime().asSeconds();
//					if(tempMod < 1)
//					{
//						tempMod += 1;
//					}

					chunk.data[selectedIndex].offset = tempMod / chunk.data[selectedIndex].secondsToBreak;
				}
			}
			else if(clipped && leftClicked)
			{
				Inventory::ItemData data = playerInventory.getSelected();
				if(data.isBlock)
				{
					playerInventory.take(data.name);
					chunk.data.push_back(Block::makeBlock(selected + offset, data.type));
					chunk.recomputeTerrarian(selected + offset, lightPos);

				}

			}

		}

		glColor3f(1.0, 1.0, 0.0);
		drawCube(lightPos, 0.5);

		glPopMatrix();

		/////DRAW SFML STUFF/////
//		glDisable(GL_FOG);
		window.pushGLStates();

		chatlog.draw(window);

		playerInventory.draw(window, invOpen);

		if(!paused)
		{
			if(!invOpen)
			{
				window.draw(up);
				window.draw(across);

			}

		}
		else
		{
			window.draw(pausedText);

		}

		sf::Text frameText(toString((int)(frameRate+0.5)) + " fps", font);
		frameText.setColor(sf::Color::Red);
		frameText.move(5, 5);
		window.draw(frameText);


		window.popGLStates();
//		glEnable(GL_FOG);

		window.display();

		frameRate = 1.0f/frameTimer.restart().asSeconds();


		/*if(frame%4 == 0)
		{
			frame = 0;
			sf::Packet packet;
			packet << -camPos.x;
			float y = (-camPos.y+eyeLevel);
			packet << y;
			packet << -camPos.z;
			packet << camRot.y;
			packet << camRot.x;
			socket.send(packet);

		}*/

	}

	return 0;

}
