#ifndef AABB_H_
#define AABB_H_

#include <SFML/Graphics.hpp>
#include <math.h>
#include <iostream>

class AABB
{
public:
	AABB()
	{
	}

	AABB(sf::Vector3f center,
			sf::Vector3f halfs) :
				c(center),
				h(halfs)
	{
	}

	sf::Vector3f getMin()
	{
		return sf::Vector3f(c.x-h.x, c.y-h.y, c.z-h.z);

	}

	sf::Vector3f getMax()
	{
		return sf::Vector3f(c.x+h.x, c.y+h.y, c.z+h.z);

	}

	bool intersects(AABB& other)
	{
		AABB intersection;
		return intersects(other, intersection);

	}

	bool intersects(AABB& other, AABB& intersection)
	{
		sf::Vector3f min = getMin();
		sf::Vector3f max = getMax();

		sf::Vector3f otherMin = other.getMin();
		sf::Vector3f otherMax = other.getMax();

		sf::Vector3f interMin(fmax(min.x, otherMin.x),
				fmax(min.y, otherMin.y),
				fmax(min.z, otherMin.z));

		sf::Vector3f interMax(fmin(max.x, otherMax.x),
				fmin(max.y, otherMax.y),
				fmin(max.z, otherMax.z));

		if(interMin.x < interMax.x
				&& interMin.y < interMax.y
				&& interMin.z < interMax.z)
		{
			intersection = AABB(interMin, interMax - interMin);
			return true;

		}

		intersection = AABB();
		return false;

	}

	sf::Vector3f getMTV()
	{
		sf::Vector3f mtv;
		float min = fmin(h.x, fmin(h.y, h.z));
		if(min == h.x)
			mtv.x = h.x;
		else if(min == h.y)
			mtv.y = h.y;
		else
			mtv.z = h.z;

		return mtv;

	}

	sf::Vector3f c;
	sf::Vector3f h;

};

#endif /* AABB_H_ */
