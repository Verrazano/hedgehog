#ifndef SKYBOX_H_
#define SKYBOX_H_

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

class Skybox
{
public:
	Skybox()
	{
		top.loadFromFile("skytop.png");

		glGenTextures(1, &topTex);
		glBindTexture(GL_TEXTURE_2D, topTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				top.getSize().x, top.getSize().y,
				0, GL_RGBA, GL_UNSIGNED_BYTE, top.getPixelsPtr());

		bot.loadFromFile("skybot.png");

		glGenTextures(1, &botTex);
		glBindTexture(GL_TEXTURE_2D, botTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				bot.getSize().x, bot.getSize().y,
				0, GL_RGBA, GL_UNSIGNED_BYTE, bot.getPixelsPtr());

		leftright.loadFromFile("skyside1.png");

		glGenTextures(1, &leftTex);
		glBindTexture(GL_TEXTURE_2D, leftTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				leftright.getSize().x, leftright.getSize().y,
				0, GL_RGBA, GL_UNSIGNED_BYTE, leftright.getPixelsPtr());

		rightTex = leftTex;

		front.loadFromFile("skyside2.png");

		glGenTextures(1, &frontTex);
		glBindTexture(GL_TEXTURE_2D, frontTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				front.getSize().x, front.getSize().y,
				0, GL_RGBA, GL_UNSIGNED_BYTE, front.getPixelsPtr());

		back.loadFromFile("skyside3.png");

		glGenTextures(1, &backTex);
		glBindTexture(GL_TEXTURE_2D, backTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				back.getSize().x, back.getSize().y,
				0, GL_RGBA, GL_UNSIGNED_BYTE, back.getPixelsPtr());

		leftTex = rightTex = backTex = frontTex;

	}

	void draw()
	{
		glPushMatrix();
		//glTranslatef(0.0, 8.0, 0.0);
		glColor3f(1.0f, 1.0f, 1.0f);

		glBindTexture(GL_TEXTURE_2D, frontTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBegin(GL_QUADS);

		//front
		glNormal3f(0.0f, 0.0f, 0.1f);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(48.0f, 48.0f, 48.0f);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(48.0f, -48.0f, 48.0f);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-48.0f, -48.0f, 48.0f);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-48.0f, 48.0f, 48.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, rightTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBegin(GL_QUADS);

		//right
		glNormal3f(0.1f, 0.0f, 0.0f);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(48.0f, 48.0f, 48.0f);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(48.0f, -48.0f, 48.0f);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(48.0f, -48.0f, -48.0f);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(48.0f, 48.0f, -48.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, backTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBegin(GL_QUADS);

		//back
		glNormal3f(0.0f, 0.0f, 0.1f);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(48.0f, 48.0f, -48.0f);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(48.0f, -48.0f, -48.0f);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-48.0f, -48.0f, -48.0f);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-48.0f, 48.0f, -48.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, leftTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBegin(GL_QUADS);

		//left
		glNormal3f(0.1f, 0.0f, 0.0f);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-48.0f, 48.0f, 48.0f);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-48.0f, -48.0f, 48.0f);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-48.0f, -48.0f, -48.0f);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-48.0f, 48.0f, -48.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, topTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBegin(GL_QUADS);

		//top
		glNormal3f(0.0f, 0.1f, 0.0f);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(48.0f, 47.0f, 48.0f);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-48.0f, 47.0f, 48.0f);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-48.0f, 47.0f, -48.0f);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(48.0f, 47.0f, -48.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, botTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBegin(GL_QUADS);

		//bottom
		glNormal3f(0.0f, -0.1f, 0.0f);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(48.0f, -48.0f, 48.0f);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-48.0f, -48.0f, 48.0f);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-48.0f, -48.0f, -48.0f);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(48.0f, -48.0f, -48.0f);

		glEnd();
		glPopMatrix();

	}

	sf::Image top;
	sf::Image bot;
	sf::Image front;
	sf::Image leftright;
	sf::Image back;

	GLuint topTex;
	GLuint botTex;
	GLuint frontTex;
	GLuint leftTex;
	GLuint rightTex;
	GLuint backTex;

};

#endif /* SKYBOX_H_ */
