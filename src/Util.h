#ifndef UTIL_H_
#define UTIL_H_

#include <math.h>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <sstream>

const float WORLD_SIZE = 4.0f;
const float CHUNK_SIZE = 16.0f;
const unsigned int RENDER_DIST = 3;

struct Vert
{
	sf::Vector3f pos;
	sf::Vector2f tex;
	sf::Vector3f n;

};


unsigned int computeHash(sf::Vector3f pos)
{
	unsigned int hash = (int)(pos.x/CHUNK_SIZE) << 16 | (int)(pos.z/CHUNK_SIZE);
	return hash;

}

unsigned int computeHash(sf::Vector2f pos)
{
	unsigned int hash = (int)(pos.x/CHUNK_SIZE) << 16 | (int)(pos.y/CHUNK_SIZE);
	return hash;

}

float lerp(float a, float b, float w)
{
	return (1.0 - w)*a + w*b;

}

std::string toString(float f)
{
	std::stringstream ss;
	ss << f;
	std::string s;
	ss >> s;
	return s;

}

sf::Vector3f cross(sf::Vector3f a, sf::Vector3f b)
{
	return sf::Vector3f(a.y*b.z - a.z*b.y,
			a.z*b.x - a.x*b.z,
			a.x*b.y - a.y*b.x);

}

float dot(sf::Vector3f a, sf::Vector3f b)
{
	return (a.x*b.x) + (a.y*b.y) + (a.z*b.z);

}

sf::Vector3f normalize(sf::Vector3f vec)
{
	float len = sqrt(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2));
	vec.x /= len;
	vec.y /= len;
	vec.z /= len;
	return vec;

}

sf::Vector2f normalize(sf::Vector2f vec)
{
	float len = sqrt(pow(vec.x, 2) + pow(vec.y, 2));
	vec.x /= len;
	vec.y /= len;
	return vec;

}

sf::Vector2f randUnitVec2()
{
	float a = rand()%360;
	a = a/180.0f*3.14159;
	float x = cos(a);
	float z = sin(a);
	return normalize(sf::Vector2f(x, z));

}

class Plane
{
public:
	float a;
	float b;
	float c;
	float d;

	Plane()
	{
		a = b = c = d = 0.0;

	}

	Plane(sf::Vector3f x, //point on the plane
			sf::Vector3f n)
	{
		n = normalize(n);
		a = n.x;
		b = n.y;
		c = n.z;
		d = -dot(x, n);

	}

	sf::Vector3f intersect(sf::Vector3f v)
	{
		return intersect(v, v*2.0f);

	}

	sf::Vector3f intersect(sf::Vector3f v1,
			sf::Vector3f v2)
	{
		sf::Vector3f dif = v1 - v2;
		float den = a*dif.x + b*dif.y + c*dif.z;
		if(den == 0.0)
		{
			return (v1 + v2)*0.5f;

		}

	    float u = (a*v1.x + b*v1.y + c*v1.z + d)/den;
	    return (v1 + u*(v2 - v1));

	}

	float signedDist(sf::Vector3f pos)
	{
		return (a*pos.x + b*pos.y + c*pos.z + d);

	}

};

void print(sf::Vector3f vec)
{
	std::cout << vec.x << ", " << vec.y << ", " << vec.z << "\n";

}

void print(sf::Vector2f vec)
{
	std::cout << vec.x << ", " << vec.y << "\n";

}


sf::Vector3f getViewAngle(sf::Vector3f camRot)
{
	sf::Vector3f view(0, 0, -1);

	float ratio = M_PI/180.0f;
	float va = camRot.x*ratio;
	float ha = -1.0*camRot.y*ratio;

	float newY = (view.y*cos(va))-(view.z*sin(va));
	float newZ = (view.y*sin(va))-(view.z*cos(va));
	view = sf::Vector3f(view.x, newY, newZ);

	float newX = (view.z*sin(ha))+(view.x*cos(ha));
	newZ = (view.z*cos(ha)-view.x*sin(ha));
	view = sf::Vector3f(newX, view.y, newZ);
	view *= -1.0f;

	return normalize(view);

}

float quickDist(sf::Vector3f a, sf::Vector3f b)
{
	sf::Vector3f d = a - b;
	return abs(d.x) + abs(d.y) + abs(d.z);

}

float dist(sf::Vector3f a, sf::Vector3f b)
{
	return sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2) + pow(b.z - a.z, 2));

}

void drawCube(sf::Vector3f pos, float size)
{
	glPushMatrix();
	glTranslatef(pos.x, pos.y, pos.z);
	glBegin(GL_QUADS);

	//front
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-size, -size, size);
	glVertex3f(-size, size, size);
	glVertex3f(size, size, size);
	glVertex3f(size, -size, size);

	//right
	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(size, size, size);
	glVertex3f(size, size, -size);
	glVertex3f(size, -size, -size);
	glVertex3f(size, -size, size);

	//backwindow.draw()
	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(-size, -size, -size);
	glVertex3f(-size, size, -size);
	glVertex3f(size, size, -size);
	glVertex3f(size, -size, -size);

	//left
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-size, size, size);
	glVertex3f(-size, size, -size);
	glVertex3f(-size, -size, -size);
	glVertex3f(-size, -size, size);

	//top
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(size, size, size);
	glVertex3f(-size, size, size);
	glVertex3f(-size, size, -size);
	glVertex3f(size, size, -size);

	//bottom
	glNormal3f(0.0f, -1.0f, 0.0f);
	glVertex3f(size, -size, size);
	glVertex3f(-size, -size, size);
	glVertex3f(-size, -size, -size);
	glVertex3f(size, -size, -size);

	glEnd();

	glPopMatrix();

}

void drawVert(Vert& v)
{
	glVertex3f(v.pos.x, v.pos.y, v.pos.z);

}

void renderShadowVolume(Vert* face,
		sf::Vector3f lightPos)
{
	Vert shadow[4];
	for(unsigned int i = 0; i < 4; i++)
	{
		shadow[i].pos = face[i].pos - lightPos;
		shadow[i].pos = normalize(shadow[i].pos);
		shadow[i].pos *= 100.0f;
		shadow[i].pos += lightPos;

	}

	//now render the shadow volume
	glBegin(GL_QUADS);
	drawVert(shadow[3]);
	drawVert(shadow[2]);
	drawVert(shadow[1]);
	drawVert(shadow[0]);
	glEnd();

	/* front cap */
	glBegin(GL_QUADS);
	drawVert(face[0]);
	drawVert(face[1]);
	drawVert(face[2]);
	drawVert(face[3]);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	drawVert(face[0]);
	drawVert(shadow[0]);
	for(unsigned int i = 1; i <= 4; i++)
	{
		drawVert(face[i % 4]);
		drawVert(shadow[i % 4]);
	}
	glEnd();

}

void drawShadowOrtho()
{
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, 1, 1, 0, 0, 1);
	glDisable(GL_DEPTH_TEST);

	glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
	glBegin(GL_QUADS);
		glVertex2i(0, 0);
		glVertex2i(0, 1);
		glVertex2i(1, 1);
		glVertex2i(1, 0);
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

#endif /* UTIL_H_ */
