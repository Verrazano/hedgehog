#ifndef INVENTORY_H_
#define INVENTORY_H_

#include "Item.h"
#include "Util.h"
#include <iostream>
#include <SFML/Audio.hpp>

class Inventory
{
public:
	Inventory(sf::Vector2u size,
			sf::Font font) :
				size(size),
				font(font)
	{
		origPos = -1;

		itemTex.loadFromFile("spritesheet_items.png");
		worldTex.loadFromFile("world.png");

		data.reserve(size.x*size.y);
		sf::RectangleShape box(sf::Vector2f(48, 48));
		box.setOrigin(24, 24);
		box.setPosition(854/2 - size.x/2*48, 480-48);
		box.setFillColor(sf::Color(100, 100, 100));
		box.setOutlineColor(sf::Color::Black);
		box.setOutlineThickness(2.0f);

		selector = box;
		selector.setFillColor(sf::Color::Transparent);
		selector.setOutlineColor(sf::Color::Yellow);

		itemGetBuffer.loadFromFile("drawKnife2.ogg");
		itemGetSound.setBuffer(itemGetBuffer);
		itemGetSound.setVolume(85);

		for(unsigned int i = 0; i < size.x; i++)
		{
			data.push_back(ItemData());
			hotbar.push_back(box);
			box.move(48, 0);

		}

		box.setPosition(854/2 - size.x/2*48, 480 - (96*2) - size.y/2*48);

		for(unsigned int i = 0; i < size.y; i++)
		{
			for(unsigned int j = 0; j < size.x; j++)
			{
				data.push_back(ItemData());
				inv.push_back(box);
				box.move(48, 0);

			}

			box.setPosition(854/2 - size.x/2*48, box.getPosition().y);
			box.move(0, 48);

		}

		selectedSlot = 0;

	}

	typedef struct ItemData
	{
		ItemData()
		{
			amount = 0;
			isBlock = false;
			type = Block::Dirt;

		}

		std::string name;
		unsigned int amount;
		sf::RectangleShape sprite;
		sf::Text text;
		bool isBlock;
		Block::Type type;
		Item copyme;


	} ItemData;

	void update(sf::Event event,
			bool open = false)
	{
		if(!open && origPos != -1)
		{
			origPos = -1;
			moving = ItemData();
			std::cout << "deal with this shit, see Inventory:update\n";

		}

		if(event.type == sf::Event::MouseWheelMoved)
		{
			int d = event.mouseWheel.delta;
			if(d <= -1)
			{
				selectedSlot++;
				selectedSlot %= size.x;

			}
			else if(d >= -1)
			{
				selectedSlot--;
				if(selectedSlot < 0)
				{
					selectedSlot = size.x;

				}

			}

		}
		else if(open && event.type == sf::Event::MouseButtonPressed)
		{
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2f mPos(event.mouseButton.x, event.mouseButton.y);
				if(origPos == -1)
				{
					for(unsigned int i = 0; i < data.size(); i++)
					{
						if(data[i].amount != 0
								&& data[i].sprite.getGlobalBounds().contains(mPos))
						{
							origPos = i;
							moving = data[i];
							std::cout << "clicking on: " << data[i].name << "\n";
							data[i] = ItemData();

						}

					}

				}
				else
				{
					for(unsigned int i = 0; i < hotbar.size(); i++)
					{
						if(hotbar[i].getGlobalBounds().contains(mPos))
						{
							for(unsigned int j = 0; j < data.size(); j++)
							{
								if(data[j].amount != 0
										&& data[j].sprite.getPosition() == hotbar[i].getPosition())
								{
									origPos = i;
									ItemData temp = data[i];
									moving.sprite.setPosition(hotbar[i].getPosition());
									moving.text.setPosition(moving.sprite.getPosition());
									moving.text.move(8, 8);
									data[i] = moving;
									moving = temp;
									return;

								}

							}

							moving.sprite.setPosition(hotbar[i].getPosition());
							moving.text.setPosition(moving.sprite.getPosition());
							moving.text.move(8, 8);
							data[i] = moving;
							origPos = -1;
							return;

						}

					}

					for(unsigned int i = 0; i < inv.size(); i++)
					{
						if(inv[i].getGlobalBounds().contains(mPos))
						{
							for(unsigned int j = 0; j < data.size(); j++)
							{
								if(data[j].amount != 0
										&& data[j].sprite.getPosition() == inv[i].getPosition())
								{
									origPos = j;
									ItemData temp = data[j];
									moving.sprite.setPosition(inv[i].getPosition());
									moving.text.setPosition(moving.sprite.getPosition());
									moving.text.move(8, 8);
									data[j] = moving;
									moving = temp;
									return;

								}

							}

							moving.sprite.setPosition(inv[i].getPosition());
							moving.text.setPosition(moving.sprite.getPosition());
							moving.text.move(8, 8);
							data[i+hotbar.size()] = moving;
							origPos = -1;
							return;


						}

					}

				}

			}
		}
		else
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
				selectedSlot = 0;
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
				selectedSlot = 1;
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
				selectedSlot = 2;
		}

	}


	void draw(sf::RenderWindow& window,
			bool open = false)
	{
		for(unsigned int i = 0; i < hotbar.size(); i++)
		{
			window.draw(hotbar[i]);
			if(selectedSlot == i)
			{
				selector.setPosition(hotbar[i].getPosition());

			}

		}

		if(open)
		{
			for(unsigned int i = 0; i < inv.size(); i++)
			{
				window.draw(inv[i]);

			}

		}

		unsigned int end = open ? data.size() : hotbar.size();

		for(unsigned int i = 0; i < end; i++)
		{
			window.draw(data[i].sprite);
			window.draw(data[i].text);

		}

		window.draw(selector);

		if(origPos != -1)
		{
			sf::Vector2f mPos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
			moving.sprite.setPosition(mPos);
			moving.text.setPosition(mPos);
			moving.text.move(10, 10);
			window.draw(moving.sprite);
			window.draw(moving.text);

		}

	}

	bool add(Item item)
	{
		int availPos = -1;
		for(unsigned int i = 0; i < data.size(); i++)
		{
			if(data[i].name == item.name && data[i].amount < 17)
			{
				itemGetSound.play();
				data[i].amount++;
				data[i].text.setString(toString(data[i].amount));
				return true;

			}
			else if(data[i].amount == 0 && availPos == -1)
			{
				availPos = i;

			}

		}

		if(availPos != -1)
		{
			itemGetSound.play();
			data[availPos].name = item.name;
			data[availPos].amount = 1;
			sf::RectangleShape& sprite = data[availPos].sprite;
			sprite.setSize(sf::Vector2f(32, 32));
			sprite.setOrigin(16, 16);
			data[availPos].copyme = item;
			if(item.isBlock)
			{
				data[availPos].isBlock = true;
				data[availPos].type = item.type;
				sprite.setTexture(&worldTex);

			}
			else
			{
				sprite.setTexture(&itemTex);

			}
			sf::Vector2f texPos = item.texPos;
			sprite.setTextureRect(sf::IntRect(texPos.x*1024.0, texPos.y*2048.0, 128, 128));
			if(availPos < hotbar.size())
			{
				sprite.setPosition(hotbar[availPos].getPosition());

			}
			else
			{
				sprite.setPosition(inv[availPos-hotbar.size()].getPosition());

			}
			sf::Text& text = data[availPos].text;
			text.setFont(font);
			text.setCharacterSize(14.0);
			text.setString("1");
			text.setPosition(sprite.getPosition());
			text.move(8, 8);

			return true;

		}

		return false;

	}

	void takeSelected()
	{
		if(data[selectedSlot].amount != 0)
		{
			data[selectedSlot].amount--;
			data[selectedSlot].text.setString(toString(data[selectedSlot].amount));
			if(data[selectedSlot].amount == 0)
				data[selectedSlot] = ItemData();

		}

	}

	bool take(std::string name)
	{
		for(unsigned int i = 0; i < data.size(); i++)
		{
			if(data[i].name == name)
			{
				data[i].amount--;
				data[i].text.setString(toString(data[i].amount));
				if(data[i].amount == 0)
					data[i] = ItemData();

				return true;

			}

		}

		return false;

	}

	ItemData& getSelected()
	{
		return data[selectedSlot];

	}

	sf::SoundBuffer itemGetBuffer;
	sf::Sound itemGetSound;

	sf::Texture itemTex;
	sf::Texture worldTex;

	sf::RectangleShape selector;
	int selectedSlot;
	std::vector<ItemData> data;
	sf::Vector2u size;
	sf::Font font;

	ItemData moving;
	int origPos;

	std::vector<sf::RectangleShape> hotbar;

	std::vector<sf::RectangleShape> inv;

};

#endif /* INVENTORY_H_ */
